#!/bin/bash
unset urls
declare -a urls=( 'https://calendar.google.com/calendar/ical/tsranso%40g.clemson.edu/private-ded420c8a6cc9c6d955734f7556b6c72/basic.ics' 'https://calendar.google.com/calendar/ical/ransomtim8078%40gmail.com/private-d6a357ce609183e1072a9d6c75294e7b/basic.ics' 'https://calendar.google.com/calendar/ical/c_g0p9ls4iuhdhqn5v2gl0i76dbk%40group.calendar.google.com/public/basic.ics' 'https://clemson.instructure.com/feeds/calendars/user_AiXnkr1srwALwSMsLWlZP4nAabwZq9l9zKE9dupr.ics' )
unset names
declare -a names=( 'tsranso' 'ransomtim8078' 'zoom-office' 'canvas' )
WGET=$(which wget)
ICS2ORG=/home/tsranso/.emacs.d/ical2org/ical2org.awk

for index in "${!urls[@]}"; do
    URL=${urls[index]};
    FILENAME=${names[index]};
    CAL_DIR=/home/tsranso/.emacs.d/org/calendars
    ICSFILE=$CAL_DIR/$FILENAME.ics
    ORGFILE=$CAL_DIR/$FILENAME.org

    $WGET -O $ICSFILE $URL
    AUTHOR="Tim Ransom" EMAIL="tsranso@clemson.edu" TITLE="$FILENAME google calendar" CALENDAR="$FILENAME" FILETAGS="calendar"  $ICS2ORG < $ICSFILE > $ORGFILE
done
